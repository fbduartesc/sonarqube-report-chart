# Sonarqube report chart

## Run local

The file **script.sh** will be executable.

Follow the step:

```sh
user@linux:~$ chmod +x ./script.sh
```

To run local and to generate JSON file, you need sonarqube **TOKEN** and the **URL** sonar as parameter.

```sh
user@linux:~$ ./script.sh tedr4321fbd3f441c1e3c: https://sonarqube-api.glitch.me
```
