const LABEL = {
  ncloc: {
    original: "ncloc",
    name: "nloc",
  },
  coverage: {
    original: "coverage",
    name: "coverage",
  },
  duplicated_lines_density: {
    original: "duplicated_lines_density",
    name: "Gráfico de duplicidade",
  },
  code_smells: {
    original: "code_smells",
    name: "Gráfico de Code Smells",
  },
  bugs: {
    original: "bugs",
    name: "Gráfico de bugs",
  },
  vulnerabilities: {
    original: "vulnerabilities",
    name: "Gráfico de vulnerabilidades",
  },
  sqale_index: {
    original: "sqale_index",
    name: "sqale_index",
  },
  sqale_rating: {
    original: "sqale_rating",
    name: "sqale_rating",
  },
  reliability_rating: {
    original: "reliability_rating",
    name: "reliability_rating",
  },
  security_rating: {
    original: "security_rating",
    name: "security_rating",
  },
};

async function fetchMetricsJSON() {
  const response = await fetch("./metrics.json");
  const metrics = await response.json();
  return metrics;
}

const createChart = (key, metric) => {
  const historicoDate = metric?.history.map(({ date }) =>
    new Date(date).toLocaleDateString("pt-BR")
  );
  const historicoValue = metric.history.map(({ value }) => value);
  const ctx = document
    .getElementById(`myChart-${key}-${metric.metric}`)
    .getContext("2d");
  const myChart = new Chart(ctx, {
    type: "line",
    data: {
      labels: historicoDate,
      datasets: [
        {
          label: LABEL[metric.metric].name,
          data: historicoValue,
          backgroundColor: [
            "rgba(255, 99, 132, 0.2)",
            "rgba(54, 162, 235, 0.2)",
            "rgba(255, 206, 86, 0.2)",
            "rgba(75, 192, 192, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
          ],
          borderColor: [
            "rgba(255, 99, 132, 1)",
            "rgba(54, 162, 235, 1)",
            "rgba(255, 206, 86, 1)",
            "rgba(75, 192, 192, 1)",
            "rgba(153, 102, 255, 1)",
            "rgba(255, 159, 64, 1)",
          ],
          borderWidth: 1,
        },
      ],
    },
    options: {
      scales: {
        y: {
          beginAtZero: true,
        },
      },
    },
  });
};

const searchData = (data) => {
  const extractKey = data.target.id.split("-");
  fetchMetricsJSON().then((result, index) => {
    const search = result.filter(({ key }) => key === extractKey[0])[0];
    let container;
    if (search) {
      search?.data.forEach((metric, indexMetric) => {
        if (
          !document.getElementById(`myChart-${search.key}-${metric.metric}`)
        ) {
          if (indexMetric === 0) {
            container = document.createElement("div");
            container.setAttribute("style", "display: flex; flex-wrap: wrap;");
          }
          const div = document.createElement("div");
          const to = document.createElement("canvas");
          to.setAttribute("id", `myChart-${search.key}-${metric.metric}`);
          to.setAttribute("width", "500");
          to.setAttribute("height", "300");
          div.appendChild(to);
          container.appendChild(div);
          const element1 = document.getElementById(`${extractKey[0]}`);
          element1.appendChild(container);

          createChart(search.key, metric);
        }
      });
    }
  });
};

fetchMetricsJSON().then((result) => {
  result.forEach((data, index) => {
    const titleDiv = document.createElement("div");
    titleDiv.setAttribute("id", `${data.key}`);
    const para = document.createElement("h2");
    const link = document.createElement("a");
    link.setAttribute("onmouseover", "this.style.cursor='pointer'");
    link.setAttribute("id", `${data.key}-${index}`);
    link.addEventListener("click", searchData);
    para.appendChild(link);
    const node = document.createTextNode(data.name);
    link.appendChild(node);
    titleDiv.appendChild(para);
    const element1 = document.getElementById(`title`);
    element1.appendChild(titleDiv);
  });
});
