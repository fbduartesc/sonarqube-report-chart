#!/bin/bash -e

# Search all projects at the sonarqube

if [ "$1" == "" ]; then
  echo "Parameter 1 TOKEN is empty"
  exit 1
fi

if [ "$2" == "" ]; then
  echo "Parameter 2 URL is empty"
  exit 1
fi
#
# List options
# bugs,vulnerabilities,sqale_index,duplicated_lines_density,
# ncloc,coverage,code_smells,reliability_rating,security_rating,sqale_rating
#
parameters="bugs%2Cvulnerabilities%2Cduplicated_lines_density%2Ccode_smells"

listProjects=$(curl -u "$1" "$2/api/components/search_projects?ps=500&facets=reliability_rating%2Csecurity_rating%2Csecurity_review_rating%2Csqale_rating%2Ccoverage%2Cduplicated_lines_density%2Cncloc%2Calert_status%2Clanguages%2Ctags%2Cqualifier&f=analysisDate%2CleakPeriodDate&s=security_rating&asc=false" | jq '.components')
masterJson="["

# index=0

# For at the listProjects to search components with activites
for row in $(echo "${listProjects}" | jq -r '.[] | @base64'); 
do
  # index=$((index+1))
  _formatNameKey()
  {
    echo ${row} | base64 --decode | jq -r ${1}
  }
  name=$(echo "$(_formatNameKey '.name')")
  key=$(echo "$(_formatNameKey '.key')")

  # Search history by component interator
  
  searchHistory=$(curl -u "$1" "$2/api/measures/search_history?from=2021-02-26T03%3A54%3A02%2B0000&component=${key}&metrics=${parameters}&ps=1000" | jq '.measures' )
  
  dataComponentJson="["
  # For at the data components to make a new json
  for rowData in $(echo "${searchHistory}" | jq -r '.[] | @base64');  
  do
    _formatResult()
    {
      echo ${rowData} | base64 --decode | jq -r ${1}
    }    
    dataComponentJson+=$(echo "$(_formatResult)")
    dataComponentJson+=","
  done

  # Remove comma
  dataJson=$(echo "${dataComponentJson::-1}") 
  dataJson+="]"
  
  result2="{\"key\": \"${key}\", \"name\": \"${name}\", \"data\": ${dataJson}}"
  
  masterJson+=${result2}
  masterJson+=","
done

file=$(echo "${masterJson::-1}") 
file+="]"
echo "${file}"  > metrics.json